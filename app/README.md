[![Logo](https://www.codev-it.at/logo.png)](https://www.codev-it.at)

# Codev-IT GoLang Boilerplate

[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://bitbucket.org/codev-it/golang-boilerplate/src/master/app/LICENSE)

## Description

Is a modern web application featuring a React-based frontend and robust API
endpoints implemented in GoLang.

### Features

* **React Frontend:** A dynamic and responsive user interface built with React.
* **Golang API Endpoint:** A high-performance backend API developed in Golang,
  optimized for speed and efficiency in handling concurrent requests.

## Contributing

Contributions are welcome. Please read our contributing guidelines and submit
pull requests to our repository.

## Donations

Your support is appreciated! You can make a donation using the following
methods:

### Stripe

For donations via credit or debit card, please visit our Stripe donation page:
[Donate with Stripe](https://donate.stripe.com/7sIaFa9DK18s8Ra8ww)

### PayPal

To donate using PayPal, please use the following link:
[Donate with PayPal](https://www.paypal.com/donate/?hosted_button_id=2GKPNJBQTAB3Y)

### Cryptocurrency

We also accept donations in cryptocurrency. Please send your contributions to
our crypto account:

- **Bitcoin (BTC):** `bc1qadle9lzzv5plw98n0gndycx0dta2zyca9rmaxe`
- **Ethereum (ETH):** `0xf7a42B686437FA1b321708802296342D582576a0`

## Contact

For any inquiries, please contact us
at [office@codev-it.at](mailto:office@codev-it.at).

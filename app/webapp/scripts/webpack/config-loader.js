const fs = require('fs');
// eslint-disable-next-line import/no-extraneous-dependencies
const yaml = require('yaml');

module.exports = () => yaml.parse(fs.readFileSync('config.yml', 'utf8')) || {};

#!/bin/sh

# debug
if [ "${DEBUG}" = true ]; then
  set -eux
else
  set -e
fi

# default
DOCKER_SOCKET=${DOCKER_SOCKET:-/var/run/docker.sock}

# add user to docker group by existing docker socket
if [ -S "${DOCKER_SOCKET}" ]; then
  addgroup -S -g "$(stat -c '%g' "${DOCKER_SOCKET}")" docker
  adduser "${BUILD_USER}" docker
  BUILD_GROUP=docker
fi

# set folder permissions
chown -R "${BUILD_USER}" /app/data

# start different user
su-exec "${BUILD_USER}":"${BUILD_GROUP}" /app/main "${@}"

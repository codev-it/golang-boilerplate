[![Logo](https://www.codev-it.at/logo.png)](https://www.codev-it.at)

# Codev-IT GoLang Boilerplate

[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://bitbucket.org/codev-it/golang-boilerplate/src/master/native/LICENSE)

## Description

This boilerplate project serves as a template for creating a Golang application
with Docker. It includes a pre-configured and pre-structured project skeleton
optimized for common Golang and Docker development tasks.

### Features
* **Golang:** The main programming language of this project is Golang, a
* statically typed, compiled language that is suited for the development of
* high-performance backend applications.
* **Docker:** We use Docker as a platform to isolate, package, and deploy
* the application. This ensures that the application runs the same way in
* every environment.

## Contributing

Contributions are welcome. Please read our contributing guidelines and submit
pull requests to our repository.

## Donations

Your support is appreciated! You can make a donation using the following
methods:

### Stripe

For donations via credit or debit card, please visit our Stripe donation page:
[Donate with Stripe](https://donate.stripe.com/7sIaFa9DK18s8Ra8ww)

### PayPal

To donate using PayPal, please use the following link:
[Donate with PayPal](https://www.paypal.com/donate/?hosted_button_id=2GKPNJBQTAB3Y)

### Cryptocurrency

We also accept donations in cryptocurrency. Please send your contributions to
our crypto account:

- **Bitcoin (BTC):** `bc1qadle9lzzv5plw98n0gndycx0dta2zyca9rmaxe`
- **Ethereum (ETH):** `0xf7a42B686437FA1b321708802296342D582576a0`

## Contact

For any inquiries, please contact us
at [office@codev-it.at](mailto:office@codev-it.at).
